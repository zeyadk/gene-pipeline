bio==0.5.0
biopython==1.79
certifi==2021.5.30
chardet==4.0.0
cloudpickle==1.6.0
dask==2021.7.0
fsspec==2021.6.1
gtfparse==1.2.1
idna==2.10
locket==0.2.1
numpy==1.19.5
pandas==1.3.0
partd==1.2.0
plac==1.3.3
python-dateutil==2.8.1
pytz==2021.1
PyYAML==5.4.1
requests==2.25.1
scikit-allel==1.3.5
six==1.16.0
toolz==0.11.1
urllib3==1.26.6
