# Gene Pipeline
A pipeline for anaylsing and plotting the difference in masses of mutated genes of a given chromosome. Python was used for reading and analysing the data. R was used for plotting the results.
A bash script connects the *pipeline*.
# Installation
To run the scripts and show the plots using R, make sure you are on linux. If you are on windows you can activate an environment and display the output in the terminal.
There are 2 scripts in the scripts folder. One for installing dependencies which is optional, and one for running the program.

## Linux
The installation scripts combines Python and R environment setup and dependencies installation in one script. So, if you know your way around virtual environments, install the requirements.txt file and [pacman](https://rdocumentation.org/packages/pacman/versions/0.5.1), then head to the next section.

1. Install Python and R on your machine.
2. Give necessary permissions for running the script.
```sh
$ chmod +x scripts/install.sh
```
3. Run the install script in the scripts folder to install required dependencies. 
```sh
$ . scripts/install.sh
```
	If it results in a permission error, that is probably R. Try running the following commands.
	```sh
	$ sudo chmod 777 /usr/local/lib/R/site-library
	$ . scripts/install.sh
	```
## Windows
1. From the command line create a virtual environment and activate.
```sh
> python -m venv .venv
> .venv\Scripts\activate
```

2. Install dependencies.
```sh
> pip install -r requirements.txt
```

# Usage

## Linux
For running the program:

1. Give necessary permissions for running the script.
```sh
$ chmod +x scritps/run.sh
```

2. Run the script and pass the chromosome number and path to fasta file
```sh
$ . scripts/run.sh 22 data/chr22.fa
```

## Windows
```sh
> python main.py 22 data/chr22.fa
```